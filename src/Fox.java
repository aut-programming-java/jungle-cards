/**
 * This class inherits of Animal
 * And implement of Injurer
 */

public class Fox extends Animal implements Injurer{
    public static final int ENERGY_FOX_INJURING=90;

    /**
     * Constructor for class Fox
     */
    public Fox(){
        name="Fox";
        life=400;
        energy=600;
    }

    /**
     * This method run action 1 on a animal
     * if a animal has not enough energy for this job return 0
     * else return damage of this action
     * @return damage of this action.And if this action not available return 0.
     */
    @Override
    public int act1() {
        if(checkEnergy(ENERGY_FOX_INJURING)){
            return injuring();
        }
        return 0;
    }

    /**
     * This method run action 2 on a animal
     * if a animal has not enough energy for this job return 0
     * else return damage of this action
     * if a animal has only one action,this method return 0.
     * @return damage of this action.And if this action not available return 0.
     */
    @Override
    public int act2() {
        return 0;
    }

    /**
     * This method changes energy of the animal to first energy
     */
    @Override
    public void recoverEnergy() {
        energy=600;
    }

    /**
     * This method used for injuring
     * @return amount of damage of injuring
     */
    @Override
    public int injuring() {
        energy-=ENERGY_FOX_INJURING;
        return ENERGY_FOX_INJURING;
    }

    /**
     * @return amount of damage of injuring
     */
    @Override
    public int energyInjuring() {
        return ENERGY_FOX_INJURING;
    }

    /**
     * This method show details of a animal in a string
     * @return string of detail
     */
    @Override
    public String toString() {
        StringBuilder string=new StringBuilder(super.toString());
        string.append("Action 1: ");
        string.append("Injuring");
        string.append("-->");
        string.append(ENERGY_FOX_INJURING);
        string.append("\n");
        return string.toString();
    }
}
