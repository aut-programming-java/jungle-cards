/**
 * This interface uses for animals that can hurting
 *
 * @author Alirea Mazochi
 * @version 1.0.0
 * @since 1397/1/11
 */

public interface Hurter {

    /**
     * This method used for hurting
     * @return amount of damage of hurting
     */
    public int hurting();

    /**
     * @return amount of damage of hurting
     */
    public int energyHurting();

}
