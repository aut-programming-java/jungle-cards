/**
 * This class inherits of Animal
 * And implement of Hurter & Attacker
 */

public class Elephant extends Animal implements Hurter,Attacker {
    public static final int ENERGY_ELEPHANT_HURTING=70;
    public static final int ENERGY_ELEPHANT_ATTACKING=50;

    /**
     * Constructor for class Elephant
     */
    public Elephant(){
        name="Elephant";
        life=1200;
        energy=500;
    }

    /**
     * This method run action 1 on a animal
     * if a animal has not enough energy for this job return 0
     * else return damage of this action
     * @return damage of this action.And if this action not available return 0.
     */
    @Override
    public int act1() {
        if(checkEnergy(ENERGY_ELEPHANT_HURTING)){
            return hurting();
        }
        return 0;
    }

    /**
     * This method run action 2 on a animal
     * if a animal has not enough energy for this job return 0
     * else return damage of this action
     * if a animal has only one action,this method return 0.
     * @return damage of this action.And if this action not available return 0.
     */
    @Override
    public int act2() {
        if(checkEnergy(ENERGY_ELEPHANT_ATTACKING)){
            return attacking();
        }
        return 0;
    }

    /**
     * This method changes energy of the animal to first energy
     */
    @Override
    public void recoverEnergy() {
        energy=500;
    }

    /**
     * This method used for attacking
     * @return amount of damage of attacking
     */
    @Override
    public int attacking() {
        energy-=ENERGY_ELEPHANT_ATTACKING;
        return ENERGY_ELEPHANT_ATTACKING;
    }

    /**
     * @return amount of damage of attacking
     */
    @Override
    public int energyAttacking() {
        return ENERGY_ELEPHANT_ATTACKING;
    }

    /**
     * This method used for hurting
     * @return amount of damage of hurting
     */
    @Override
    public int hurting() {
        energy-=ENERGY_ELEPHANT_HURTING;
        return ENERGY_ELEPHANT_HURTING;
    }

    /**
     * @return amount of damage of hurting
     */
    @Override
    public int energyHurting() {
        return ENERGY_ELEPHANT_HURTING;
    }

    /**
     * This method show details of a animal in a string
     * @return string of detail
     */
    @Override
    public String toString() {
        StringBuilder string=new StringBuilder(super.toString());
        string.append("Action 1: ");
        string.append("Hurting");
        string.append("-->");
        string.append(ENERGY_ELEPHANT_HURTING);
        string.append(" | ");
        string.append("Action 2: ");
        string.append("Attacking");
        string.append("-->");
        string.append(ENERGY_ELEPHANT_ATTACKING);
        string.append("\n");
        return string.toString();
    }
}
