/**
 * This interface uses for animals that can killing
 *
 * @author Alirea Mazochi
 * @version 1.0.0
 * @since 1397/1/11
 */

public interface Killer {

    /**
     * This method used for killing
     * @return amount of damage of killing
     */
    public int killing();

    /**
     * @return amount of damage of killing
     */
    public int energyKilling();

}
