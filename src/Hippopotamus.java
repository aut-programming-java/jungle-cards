/**
 * This class inherits of Animal
 * And implement of Attacker
 */

public class Hippopotamus extends Animal implements Attacker{
    public static final int ENERGY_HIPPOPOTAMUS_ATTACKING=110;

    /**
     * Constructor for class Hippopotamus
     */
    public Hippopotamus(){
        name="Hippopotamus";
        life=1000;
        energy=360;
    }

    /**
     * This method run action 1 on a animal
     * if a animal has not enough energy for this job return 0
     * else return damage of this action
     * @return damage of this action.And if this action not available return 0.
     */
    @Override
    public int act1() {
        if(checkEnergy(ENERGY_HIPPOPOTAMUS_ATTACKING)){
            return attacking();
        }
        return 0;
    }

    /**
     * This method run action 2 on a animal
     * if a animal has not enough energy for this job return 0
     * else return damage of this action
     * if a animal has only one action,this method return 0.
     * @return damage of this action.And if this action not available return 0.
     */
    @Override
    public int act2() {
        return 0;
    }

    /**
     * This method changes energy of the animal to first energy
     */
    @Override
    public void recoverEnergy() {
        energy=360;
    }

    /**
     * This method used for attacking
     * @return amount of damage of attacking
     */
    @Override
    public int attacking() {
        energy-=ENERGY_HIPPOPOTAMUS_ATTACKING;
        return ENERGY_HIPPOPOTAMUS_ATTACKING;
    }

    /**
     * @return amount of damage of attacking
     */
    @Override
    public int energyAttacking() {
        return ENERGY_HIPPOPOTAMUS_ATTACKING;
    }

    /**
     * This method show details of a animal in a string
     * @return string of detail
     */
    @Override
    public String toString() {
        StringBuilder string=new StringBuilder(super.toString());
        string.append("Action 1: ");
        string.append("Attacking");
        string.append("-->");
        string.append(ENERGY_HIPPOPOTAMUS_ATTACKING);
        string.append("\n");
        return string.toString();
    }
}
