import java.util.Scanner;

/**
 * This class is a manager for game
 *
 * @author Alirea Mazochi
 * @version 1.0.0
 * @since 1397/1/11
 */
public class Game {
    Player player1;
    Player player2;

    /**
     * Constructor for class Game
     * Every game needs to two player
     *
     */
    public Game(){
        boolean flag=true;
        while(flag){
            flag=false;
            Scanner scanner=new Scanner(System.in);
            System.out.println("Select Type of game!");
            System.out.println("1.Two players");
            System.out.println("2.One Player");
            int choice=scanner.nextInt();
            switch (choice){
                case 1:
                    player1=new HumanPlayer();
                    player2=new HumanPlayer();
                    break;
                case 2:
                    player1=new HumanPlayer();
                    player2=new ComputerPlayer();
                    break;
                case 3:
                    flag=true;
                    System.out.println("ERROR::Range of input not valid!");
                    break;
            }

        }
    }

    /**
     * When a game starts,
     * Two players should play with order
     *
     */
    public void start(){
        while(true) {
            //Turn:Player 1
            showAllCards();
            System.out.println("Turn: Player 1");
            player1.turn(player2);
            if(player2.hasAnimal()==false){
                System.out.println("Player 1 win!");
                break;
            }

            //Turn:player 2
            showAllCards();
            System.out.println("Turn: player 2");
            player2.turn(player1);
            if(player1.hasAnimal()==false){
                System.out.println("Player 2 win!");
                break;
            }

        }
    }

    /**
     * With this method, we should show players' cards
     */
    public void showAllCards(){
        System.out.println("Player1's card(s)");
        player1.showCards();
        System.out.println();

        System.out.println("Player2's card(s)");
        player2.showCards();
        System.out.println();
    }
}
