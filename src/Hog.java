/**
 * This class inherits of Animal
 * And implement of Hurter
 */

public class Hog extends Animal implements Hurter {
    public static final int ENERGY_HOG_HURTING=80;

    /**
     * Constructor for class Hog
     */
    public Hog(){
        name="Hog";
        life=1100;
        energy=500;
    }

    /**
     * This method run action 1 on a animal
     * if a animal has not enough energy for this job return 0
     * else return damage of this action
     * @return damage of this action.And if this action not available return 0.
     */
    @Override
    public int act1() {
        if(checkEnergy(ENERGY_HOG_HURTING)){
            return hurting();
        }
        return 0;
    }

    /**
     * This method run action 2 on a animal
     * if a animal has not enough energy for this job return 0
     * else return damage of this action
     * if a animal has only one action,this method return 0.
     * @return damage of this action.And if this action not available return 0.
     */
    @Override
    public int act2() {
        return 0;
    }

    /**
     * This method changes energy of the animal to first energy
     */
    @Override
    public void recoverEnergy() {
        energy=500;
    }

    /**
     * This method used for hurting
     * @return amount of damage of hurting
     */
    @Override
    public int hurting() {
        energy-=ENERGY_HOG_HURTING;
        return ENERGY_HOG_HURTING;
    }

    /**
     * @return amount of damage of hurting
     */
    @Override
    public int energyHurting() {
        return ENERGY_HOG_HURTING;
    }

    /**
     * This method show details of a animal in a string
     * @return string of detail
     */
    @Override
    public String toString() {
        StringBuilder string=new StringBuilder(super.toString());
        string.append("Action 1: ");
        string.append("Hurting");
        string.append("-->");
        string.append(ENERGY_HOG_HURTING);
        string.append("\n");
        return string.toString();
    }
}
