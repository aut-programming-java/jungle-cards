/**
 * This interface uses for animals that can injuring
 *
 * @author Alirea Mazochi
 * @version 1.0.0
 * @since 1397/1/11
 */

public interface Injurer {

    /**
     * This method used for injuring
     * @return amount of damage of injuring
     */
    public int injuring();

    /**
     * @return amount of damage of injuring
     */
    public int energyInjuring();
}
