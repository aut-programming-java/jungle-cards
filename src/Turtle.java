/**
 * This class inherits of Animal
 * And implement of Biter
 */
public class Turtle extends Animal implements Biter {
    public static final int ENERGY_TURTLE_BITING=200;

    /**
     * Constructor for class Turtle
     */
    public Turtle(){
        name="Turtle";
        life=350;
        energy=230;
    }

    /**
     * This method run action 1 on a animal
     * if a animal has not enough energy for this job return 0
     * else return damage of this action
     * @return damage of this action.And if this action not available return 0.
     */
    @Override
    public int act1() {
        if(checkEnergy(ENERGY_TURTLE_BITING)){
            return biting();
        }
        return 0;
    }

    /**
     * This method run action 2 on a animal
     * if a animal has not enough energy for this job return 0
     * else return damage of this action
     * if a animal has only one action,this method return 0.
     * @return damage of this action.And if this action not available return 0.
     */
    @Override
    public int act2() {
        return 0;
    }

    /**
     * This method changes energy of the animal to first energy
     */
    @Override
    public void recoverEnergy() {
        energy=230;
    }

    /**
     * This method used for biting
     * @return amount of damage of biting
     */
    @Override
    public int biting() {
        energy-=ENERGY_TURTLE_BITING;
        return ENERGY_TURTLE_BITING;
    }

    /**
     * @return amount of damage of biting
     */
    @Override
    public int energyBiting() {
        return ENERGY_TURTLE_BITING;
    }

    /**
     * This method show details of a animal in a string
     * @return string of detail
     */
    @Override
    public String toString() {
        StringBuilder string=new StringBuilder(super.toString());
        string.append("Action 1: ");
        string.append("Biting");
        string.append("-->");
        string.append(ENERGY_TURTLE_BITING);
        string.append("\n");
        return string.toString();
    }
}
