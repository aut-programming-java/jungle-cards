import java.util.ArrayList;
import java.util.Random;

/**
 * This class enhances Class Player.
 * This kind of player related to a Computer Player.
 * Decides of computer player related to chance
 *
 * @author Alirea Mazochi
 * @version 1.0.0
 * @since 1397/1/11
 */

public class ComputerPlayer extends Player {

    /**
     * This method uses for recovering energy a animal.
     * This method only 3 times is useful
     * @return if this method run return true.
     */
    public boolean recovering(){
        Random random=new Random();
        int choice;
        if(numRecovering==3){
            return false;
        }
        else{
            numRecovering++;
            choice=random.nextInt(animals.size());
            animals.get(choice).recoverEnergy();
            return true;

        }
    }

    /**
     * This method gives a array and the player chose part of this cards.
     * @param animalsTotal a array that has many cards.
     */
    public void choseAnimals(Animal[] animalsTotal){
        ArrayList<Integer> arrayList=new ArrayList<Integer>();
        while(arrayList.size()!=NUM_CARDS){
            Random random=new Random();
            int newNum;
            newNum=random.nextInt(NUM_CHOICE);

            if(arrayList.contains(newNum)==false){
                arrayList.add(newNum);
            }
        }
        for(int i=0 ; i<NUM_CARDS ; i++){
            animals.add(animalsTotal[arrayList.get(i)]);
        }
    }

    /**
     * This method is very important.
     * Every players decides kind of attack by this method.
     *
     * @param enemy A player that is opponent of the player.
     */
    public void turn(Player enemy){
        Random random=new Random();
        int choice;
        boolean flag=true;
        while(flag){
            choice=random.nextInt(5);
            switch (choice){
                case 0:
                case 1:
                    if(simpleAttack(enemy)) {
                        System.out.println("Computer chose \"Simple Attack\"!");
                        System.out.println();
                        flag=false;
                    }
                    break;
                case 2:
                case 3:
                    if(commonAttack(enemy)){
                        System.out.println("Computer chose \"Common Attack\"!");
                        System.out.println();
                        flag=false;
                    }
                    break;
                case 4:
                    if(recovering()){
                        System.out.println("Computer chose \"Recovering\"!");
                        System.out.println();
                        flag=false;
                    }
                    break;
            }
        }
    }

    /**
     * This method uses for common attack.
     * @param enemy
     * @return
     */
    public boolean commonAttack(Player enemy){
        System.out.println("1.Injuring");
        System.out.println("2.Killing");
        System.out.println("3.Hurting");
        System.out.println("4.Attacking");
        System.out.println("5.Biting");

        Random random=new Random();

        int choice=random.nextInt(5)+1;
        ArrayList<Animal> chosenAnimals = new ArrayList<Animal>();

        int sumEnergy=0;
        switch (choice){
            case 1:
                for(int i=0 ; i<animals.size() ; i++){
                    if(animals.get(i) instanceof  Injurer){
                        chosenAnimals.add(animals.get(i));
                        Injurer injurer=(Injurer) animals.get(i);
                        sumEnergy+=injurer.energyInjuring();
                    }
                }
                break;
            case 2:
                for(int i=0 ; i<animals.size() ; i++){
                    if(animals.get(i) instanceof  Killer){
                        chosenAnimals.add(animals.get(i));
                        Killer killer=(Killer)animals.get(i);
                        sumEnergy+=killer.energyKilling();
                    }
                }
                break;
            case 3:
                for(int i=0 ; i<animals.size() ; i++){
                    if(animals.get(i) instanceof Hurter){
                        chosenAnimals.add(animals.get(i));
                        Hurter hurter=(Hurter) animals.get(i);
                        sumEnergy+=hurter.energyHurting();
                    }
                }
                break;
            case 4:
                for(int i=0 ; i<animals.size() ; i++){
                    if(animals.get(i) instanceof Attacker){
                        chosenAnimals.add(animals.get(i));
                        Attacker attacker=(Attacker) animals.get(i);
                        sumEnergy+=attacker.energyAttacking();

                    }
                }
                break;
            case 5:
                for(int i=0 ; i<animals.size() ; i++){
                    if(animals.get(i) instanceof Biter){
                        chosenAnimals.add(animals.get(i));
                        Biter biter=(Biter) animals.get(i);
                        sumEnergy+=biter.energyBiting();
                    }
                }
        }
        if(commonAttackCheck(chosenAnimals,sumEnergy)){
            attackToEnemy(sumEnergy,enemy);
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * This method uses for simple attack
     * @param enemy A player that is opponent of the player.
     * @return If this method run, return true
     */
    public boolean simpleAttack(Player enemy){
        Random random=new Random();
        int choice;
        choice=random.nextInt(animals.size());
        int way=random.nextInt(2)+1;
        int energy;
        switch (way){
            case 1:
                energy=animals.get(choice).act1();
                if(energy>0){
                    attackToEnemy(energy,enemy);
                    return true;
                }
                break;
            case 2:
                energy=animals.get(choice).act2();
                if(energy>0){
                    attackToEnemy(energy,enemy);
                    return true;
                }
                break;
        }
        return false;
    }

    /**
     * When a animal attacks, we use this method for attack to one enemy's animal.
     * @param energy energy of attack!
     * @param enemy A player that is opponent of the player.
     */
    public void attackToEnemy(int energy, Player enemy){
        Random random=new Random();
        int choice = random.nextInt(enemy.getAnimals().size());
        if (enemy.getAnimals().get(choice).reduceLife(energy)==true){
            enemy.getAnimals().remove(choice);
        }
    }

}
