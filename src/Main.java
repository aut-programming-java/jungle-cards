/**
 * This program is a application for simulation a game
 * Name of this game is Jungle Cards
 * This is a part of norooz assignment
 * This program has a strong error handler :)
 *
 * @author Alirea Mazochi
 * @version 1.0.0
 * @since 1397/1/11
 */

public class Main {
    /**
     * This method is starting point of program
     * @param args Input values that enter in command line
     */
    public static void main(String args[]){
        Game game=new Game();
        game.start();
    }
}
