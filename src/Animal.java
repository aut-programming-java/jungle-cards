/**
 * This Class store common fields of every type of animals
 * And it can calculate many common method
 *
 * @author Alirea Mazochi
 * @version 1.0.0
 * @since 1397/1/11
 */

abstract public class Animal {
    protected int energy;
    protected int  life;
    protected String name;

    /**
     * This method run action 1 on a animal
     * if a animal has not enough energy for this job return 0
     * else return damage of this action
     * @return damage of this action.And if this action not available return 0.
     */
    abstract public int act1();

    /**
     * This method run action 2 on a animal
     * if a animal has not enough energy for this job return 0
     * else return damage of this action
     * if a animal has only one action,this method return 0.
     * @return damage of this action.And if this action not available return 0.
     */
    abstract public int act2();

    /**
     * This method changes energy of the animal to first energy
     */
    abstract public void recoverEnergy();

    /**
     * This method checks that a animal has enough energy or not
     * @param energyNeed the amount of energy that a animal needs.
     * @return if a animal has enough energy return true
     */
    public boolean checkEnergy(int energyNeed){
        if(energyNeed<=energy){
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * This method uses for common attack
     * @param energyNeed the energy that reduce of energy of animal
     */
    public void commonAttack(int energyNeed){
        energy-=energyNeed;
    }

    /**
     * This method show details of a animal in a string
     * @return string of detail
     */
    @Override
    public String toString(){
        StringBuilder string=new StringBuilder();
        string.append("Name : ");
        string.append(name);
        string.append(" | ");
        string.append("Energy : ");
        string.append(energy);
        string.append(" | ");
        string.append("Life : ");
        string.append(life);
        string.append(" | ");
        return string.toString();
    }

    /**
     * When a animal damaged we use this method
     * @param lostLife amount of damage
     * @return if a animal kills return true
     */
    public boolean reduceLife(int lostLife){
        life-=lostLife;
        if(life<=0){
            return true;
        }
        else{
            return false;
        }
    }
}
