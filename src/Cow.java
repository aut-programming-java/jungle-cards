/**
 * This class inherits of Animal
 * And implement of Injurer & Attacker
 */

public class Cow extends Animal implements Injurer,Attacker {
    public static final int ENERGY_COW_INJURING=100;
    public static final int ENERGY_COW_ATTACKING=90;

    /**
     * Constructor for class Cow
     */
    public Cow(){
        name="Cow";
        life=750;
        energy=400;
    }

    /**
     * This method run action 1 on a animal
     * if a animal has not enough energy for this job return 0
     * else return damage of this action
     * @return damage of this action.And if this action not available return 0.
     */
    @Override
    public int act1() {
        if(checkEnergy(ENERGY_COW_ATTACKING)){
            return attacking();
        }
        return 0;
    }

    /**
     * This method run action 2 on a animal
     * if a animal has not enough energy for this job return 0
     * else return damage of this action
     * if a animal has only one action,this method return 0.
     * @return damage of this action.And if this action not available return 0.
     */
    @Override
    public int act2() {
        if(checkEnergy(ENERGY_COW_INJURING)){
            return injuring();
        }
        return 0;
    }

    /**
     * This method changes energy of the animal to first energy
     */
    @Override
    public void recoverEnergy() {
        energy=400;
    }

    /**
     * This method used for attacking
     * @return amount of damage of attacking
     */
    @Override
    public int attacking() {
        energy-=ENERGY_COW_ATTACKING;
        return ENERGY_COW_ATTACKING;
    }

    /**
     * @return amount of damage of attacking
     */
    @Override
    public int energyAttacking() {
        return ENERGY_COW_ATTACKING;
    }

    /**
     * This method used for injuring
     * @return amount of damage of injuring
     */
    @Override
    public int injuring() {
        energy-=ENERGY_COW_INJURING;
        return ENERGY_COW_INJURING;
    }

    /**
     * @return amount of damage of injuring
     */
    @Override
    public int energyInjuring() {
        return ENERGY_COW_INJURING;
    }

    /**
     * This method show details of a animal in a string
     * @return string of detail
     */
    @Override
    public String toString() {
        StringBuilder string=new StringBuilder(super.toString());
        string.append("Action 1: ");
        string.append("Attacking");
        string.append("-->");
        string.append(ENERGY_COW_ATTACKING);
        string.append(" | ");
        string.append("Action 2: ");
        string.append("Injuring");
        string.append("-->");
        string.append(ENERGY_COW_INJURING);
        string.append("\n");
        return string.toString();
    }
}
