/**
 * This interface uses for animals that can biting
 *
 * @author Alirea Mazochi
 * @version 1.0.0
 * @since 1397/1/11
 */

public interface Biter {

    /**
     * This method used for biting
     * @return amount of damage of biting
     */
    public int biting();

    /**
     * @return amount of damage of biting
     */
    public int energyBiting();

}
