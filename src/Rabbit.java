/**
 * This class inherits of Animal
 * And implement of Biter
 */
public class Rabbit extends Animal implements Biter {
    public static final int ENERGY_RABBIT_BITING=80;

    /**
     * Constructor for class Rabit
     */
    public Rabbit(){
        name="Rabbit";
        life=200;
        energy=350;
    }

    /**
     * This method run action 1 on a animal
     * if a animal has not enough energy for this job return 0
     * else return damage of this action
     * @return damage of this action.And if this action not available return 0.
     */
    @Override
    public int act1() {
        if(checkEnergy(ENERGY_RABBIT_BITING)){
            return biting();
        }
        return 0;
    }

    /**
     * This method run action 2 on a animal
     * if a animal has not enough energy for this job return 0
     * else return damage of this action
     * if a animal has only one action,this method return 0.
     * @return damage of this action.And if this action not available return 0.
     */
    @Override
    public int act2() {
        return 0;
    }

    /**
     * This method changes energy of the animal to first energy
     */
    @Override
    public void recoverEnergy() {
        energy=80;
    }

    /**
     * This method used for biting
     * @return amount of damage of biting
     */
    @Override
    public int biting() {
        energy-=ENERGY_RABBIT_BITING;
        return ENERGY_RABBIT_BITING;
    }

    /**
     * @return amount of damage of biting
     */
    @Override
    public int energyBiting() {
        return ENERGY_RABBIT_BITING;
    }

    /**
     * This method show details of a animal in a string
     * @return string of detail
     */
    @Override
    public String toString() {
        StringBuilder string=new StringBuilder(super.toString());
        string.append("Action 1: ");
        string.append("Biting");
        string.append("-->");
        string.append(ENERGY_RABBIT_BITING);
        string.append("\n");
        return string.toString();
    }
}
