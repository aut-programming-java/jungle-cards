import java.util.ArrayList;
import java.util.Scanner;

/**
 * This class enhances Class Player.
 * This kind of player related to a Human Player.
 *
 * @author Alirea Mazochi
 * @version 1.0.0
 * @since 1397/1/11
 */
public class HumanPlayer extends Player {

    /**
     * This method uses for recovering energy a animal.
     * This method only 3 times is useful
     * @return if this method run return true.
     */
    public boolean recovering() {
        Scanner scanner = new Scanner(System.in);
        int choice;
        if (numRecovering == 3) {
            System.out.println("ERROR::You used this option 3 times!");
            return false;
        } else {
            numRecovering++;
            while (true) {
                System.out.println("Select a number of animal!");
                choice = scanner.nextInt();
                if (0 <= choice && choice < animals.size()) {
                    animals.get(choice).recoverEnergy();
                    return true;
                } else {
                    System.out.println("ERROR::Range of input not valid!");
                }
            }
        }
    }

    /**
     * This method gives a array and the player chose part of this cards.
     * @param animalsTotal a array that has many cards.
     */
    public void choseAnimals(Animal[] animalsTotal) {
        for (int i = 0; i < NUM_CHOICE; i++) {
            System.out.print(i+"- ");
            System.out.println(animalsTotal[i]);
        }
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        while (arrayList.size() != NUM_CARDS) {
            System.out.println("Chose number of card!");
            Scanner scanner = new Scanner(System.in);
            int newNum;
            newNum = scanner.nextInt();
            if (0 <= newNum && newNum < NUM_CHOICE) {
                if (arrayList.contains(newNum)) {
                    System.out.println("ERROR::This card been chosen before!");
                } else {
                    arrayList.add(newNum);
                }
            } else {
                System.out.println("ERROR::Range of input not valid!");
            }
        }
        for (int i = 0; i < NUM_CARDS; i++) {
            animals.add(animalsTotal[arrayList.get(i)]);
        }
    }

    /**
     * This method is very important.
     * Every players decides kind of attack by this method.
     *
     * @param enemy A player that is opponent of the player.
     */
    public void turn(Player enemy) {
        Scanner scanner = new Scanner(System.in);
        int choice;
        boolean flag = true;
        while (flag) {
            System.out.println("Select one option:");
            System.out.println("1.Simple Attacking");
            System.out.println("2.Common Attacking");
            System.out.println("3.Recovering");
            choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    if (simpleAttack(enemy)) {
                        flag = false;
                    }
                    break;
                case 2:
                    if (commonAttack(enemy)) {
                        flag = false;
                    }
                    break;
                case 3:
                    if (recovering()) {
                        flag = false;
                    }
                    break;
                default:
                    System.out.println("ERROR::Your input not valid!");
                    break;

            }
        }
    }

    /**
     * This method for choosing a group animal for common attack
     * @return a group of animals
     */
    public ArrayList<Animal> commonAttackAnimals() {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Animal> chosenAnimals = new ArrayList<>();
        int inp = 0;
        while (inp != -1) {
            System.out.println("Enter number of animal!(If finished your job enter -1)");
            inp = scanner.nextInt();
            if (inp != -1) {
                if (0 <= inp && inp < animals.size()) {
                    if (chosenAnimals.contains(inp)) {
                        System.out.println("ERROR::This animal chose before!");
                    } else {
                        chosenAnimals.add(animals.get(inp));
                    }
                } else {
                    System.out.println("ERROR::Range of input not valid!");
                }
            }
        }
        return chosenAnimals;
    }

    /**
     * This method uses for common attack.
     * @param enemy
     * @return
     */
    public boolean commonAttack(Player enemy) {
        Scanner scanner = new Scanner(System.in);

        ArrayList<Animal> chosenAnimals;
        chosenAnimals = commonAttackAnimals();

        System.out.println("Enter type of attack!");
        System.out.println("1.Injuring");
        System.out.println("2.Killing");
        System.out.println("3.Hurting");
        System.out.println("4.Attacking");
        System.out.println("5.Biting");

        int choice = scanner.nextInt();
        int sumEnergy = 0;
        boolean flag = false;
        switch (choice) {
            case 1:
                for (int i = 0; i < chosenAnimals.size(); i++) {
                    if (chosenAnimals.get(i) instanceof Injurer) {
                        Injurer injurer = (Injurer) chosenAnimals.get(i);
                        sumEnergy += injurer.energyInjuring();
                    } else {
                        System.out.println("ERROR::Every animals don't has skill injuring!");
                        flag = true;
                        break;
                    }
                }
                break;
            case 2:
                for (int i = 0; i < chosenAnimals.size(); i++) {
                    if (chosenAnimals.get(i) instanceof Killer) {
                        Killer killer = (Killer) chosenAnimals.get(i);
                        sumEnergy += killer.energyKilling();
                    } else {
                        System.out.println("ERROR::Every animals don't has skill killing!");
                        flag = true;
                        break;
                    }
                }
                break;
            case 3:
                for (int i = 0; i < chosenAnimals.size(); i++) {
                    if (chosenAnimals.get(i) instanceof Hurter) {
                        Hurter hurter = (Hurter) chosenAnimals.get(i);
                        sumEnergy += hurter.energyHurting();
                    } else {
                        System.out.println("ERROR::Every animals don't has skill hurting!");
                        flag = true;
                        break;
                    }
                }
            case 4:
                for (int i = 0; i < chosenAnimals.size(); i++) {
                    if (chosenAnimals.get(i) instanceof Attacker) {
                        Attacker attacker = (Attacker) chosenAnimals.get(i);
                        sumEnergy += attacker.energyAttacking();
                    } else {
                        System.out.println("ERROR::Every animals don't has skill attacking!");
                        flag = true;
                        break;
                    }
                }
                break;
            case 5:
                for (int i = 0; i < chosenAnimals.size(); i++) {
                    if (chosenAnimals.get(i) instanceof Biter) {
                        Biter biter = (Biter) chosenAnimals.get(i);
                        sumEnergy += biter.energyBiting();
                    } else {
                        System.out.println("ERROR::Every animals don't has skill biting!");
                        flag = true;
                        break;
                    }
                }
                break;
            default:
                flag = true;
                System.out.println("ERROR::Range of input not valid!");
                break;

        }
        if(flag){
            return false;

        }
        else{
            if(commonAttackCheck(chosenAnimals,sumEnergy)){
                attackToEnemy(sumEnergy,enemy);
                return true;

            }
            else{
                System.out.println("ERROR::Every animals have not enough energy!");
                return false;
            }
        }
    }

    /**
     * This method uses for simple attack
     * @param enemy A player that is opponent of the player.
     * @return If this method run, return true
     */
    public boolean simpleAttack(Player enemy){
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter number of animal for attack!");
        int choice;
        choice=scanner.nextInt();
        if(0<=choice && choice<animals.size()){
            System.out.println(animals.get(choice));
            System.out.println("Enter number of action!");
            int way=scanner.nextInt();
            int energy;
            switch (way){
                case 1:
                    energy=animals.get(choice).act1();
                    if(energy>0){
                        attackToEnemy(energy,enemy);
                        return true;
                    }
                    else{
                        System.out.println("This action not available for this animal!");
                    }
                    break;
                case 2:
                    energy=animals.get(choice).act2();
                    if(energy>0){
                        attackToEnemy(energy,enemy);
                        return true;
                    }
                    else{
                        System.out.println("This action not available for this animal!");
                    }
                    break;
                default:
                    System.out.println("ERROR::Input not valid!");
                    break;
            }

        }
        else{
            System.out.println("ERROR::Input not valid!");
        }
        return false;
    }

    /**
     * When a animal attacks, we use this method for attack to one enemy's animal.
     * @param energy energy of attack!
     * @param enemy A player that is opponent of the player.
     */
    public void attackToEnemy(int energy, Player enemy){
        Scanner scanner=new Scanner(System.in);
        while(true) {
            System.out.println("Witch enemy's animal attack?");
            int choice = scanner.nextInt();
            if (0 <= choice && choice < enemy.getAnimals().size()) {
                if (enemy.getAnimals().get(choice).reduceLife(energy)==true){
                    System.out.println("The enemy's animal killed!");
                    enemy.getAnimals().remove(choice);
                    break;
                }
                else{
                    System.out.println("Done!");
                    break;
                }
            } else {
                System.out.println("ERROR::This card not exit in enemy's card");
            }
        }
    }

}
