/**
 * This class inherits of Animal
 * And implement of Killer & Injurer
 */

public class Bear extends Animal implements Killer,Injurer {
    public static final int ENERGY_BEAR_KILLING=600;
    public static final int ENERGY_BEAR_INJURING=130;

    /**
     * Constructor for class Bear
     */
    public Bear(){
        name="Bear";
        life=850;
        energy=900;
    }

    /**
     * This method changes energy of the animal to first energy
     */
    @Override
    public void recoverEnergy() {
        energy=900;
    }

    /**
     * This method run action 1 on a animal
     * if a animal has not enough energy for this job return 0
     * else return damage of this action
     * @return damage of this action.And if this action not available return 0.
     */
    @Override
    public int act1() {
        if(checkEnergy(ENERGY_BEAR_INJURING)){
            return injuring();
        }
        return 0;
    }

    /**
     * This method run action 2 on a animal
     * if a animal has not enough energy for this job return 0
     * else return damage of this action
     * if a animal has only one action,this method return 0.
     * @return damage of this action.And if this action not available return 0.
     */
    @Override
    public int act2() {
        if(checkEnergy(ENERGY_BEAR_KILLING)){
            return killing();
        }
        return 0;
    }

    /**
     * This method used for injuring
     * @return amount of damage of injuring
     */
    @Override
    public int injuring() {
        energy-=ENERGY_BEAR_INJURING;
        return ENERGY_BEAR_INJURING;
    }

    /**
     * @return amount of damage of injuring
     */
    @Override
    public int energyInjuring() {
        return ENERGY_BEAR_INJURING;
    }

    /**
     * This method used for killing
     * @return amount of damage of killing
     */
    @Override
    public int killing() {
        energy-=ENERGY_BEAR_KILLING;
        return ENERGY_BEAR_KILLING;
    }

    /**
     * @return amount of damage of killing
     */
    @Override
    public int energyKilling() {
        return ENERGY_BEAR_KILLING;
    }

    /**
     * This method show details of a animal in a string
     * @return string of detail
     */
    @Override
    public String toString() {
        StringBuilder string=new StringBuilder(super.toString());
        string.append("Action 1: ");
        string.append("Injuring");
        string.append("-->");
        string.append(ENERGY_BEAR_INJURING);
        string.append(" | ");
        string.append("Action 2: ");
        string.append("Killing");
        string.append("-->");
        string.append(ENERGY_BEAR_KILLING);
        string.append("\n");
        return string.toString();
    }
}
