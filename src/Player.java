import java.util.ArrayList;
import java.util.Random;

/**
 * This class has generals features that every player should has.
 * This class a abstract class.
 *
 * @author Alirea Mazochi
 * @version 1.0.0
 * @since 1397/1/11
*/
abstract public class Player {
    protected ArrayList<Animal> animals;
    int numRecovering;
    public static final int NUM_CARDS=10;
    public static final int NUM_CHOICE=30;
    public static final int MAX_TYPE_ANIMAL=5;
    public static final int NUM_TYPE_ANIMAL=12;

    /**
     * Constructor for this class.
     * Every player has animals
     * And in start of game, they should chose many animal card.
     *
     */
    public Player(){
        animals=new ArrayList<>();
        numRecovering=0;
        Animal animalsTotal[]=createTotalCards();
        choseAnimals(animalsTotal);
    }

    /**
     * This method uses for recovering energy a animal.
     * This method only 3 times is useful
     * @return if this method run return true.
     */
    abstract public boolean recovering();

    /**
     * This method gives a array and the player chose part of this cards.
     * @param animalsTotal a array that has many cards.
     */
    abstract public void choseAnimals(Animal[] animalsTotal);

    /**
     * This method makes a lot of animal randomize
     *
     * @return a array that has many cards.
     */
    public Animal[] createTotalCards(){
        Animal animalsTotal[]=new Animal[NUM_CHOICE];
        int numberOfAnimal[]=new int[NUM_TYPE_ANIMAL];
        for(int i=0 ; i<NUM_CHOICE ; i++){
            Random random=new Random();
            int newRandomNumber=random.nextInt(NUM_TYPE_ANIMAL);
            if(numberOfAnimal[newRandomNumber]<MAX_TYPE_ANIMAL){
                numberOfAnimal[newRandomNumber]++;
                switch (random.nextInt(NUM_TYPE_ANIMAL)){
                    case 0:
                        animalsTotal[i]=new Lion();
                        break;
                    case 1:
                        animalsTotal[i]=new Bear();
                        break;
                    case 2:
                        animalsTotal[i]=new Tiger();
                        break;
                    case 3:
                        animalsTotal[i]=new Vulture();
                        break;
                    case 4:
                        animalsTotal[i]=new Fox();
                        break;
                    case 5:
                        animalsTotal[i]=new Elephant();
                        break;
                    case 6:
                        animalsTotal[i]=new Wolf();
                        break;
                    case 7:
                        animalsTotal[i]=new Hog();
                        break;
                    case 8:
                        animalsTotal[i]=new Hippopotamus();
                        break;
                    case 9:
                        animalsTotal[i]=new Cow();
                        break;
                    case 10:
                        animalsTotal[i]=new Rabbit();
                        break;
                    case 11:
                        animalsTotal[i]=new Turtle();
                        break;
                }
            }
            else{
                i--;
            }


        }
        return animalsTotal;
    }

    /**
     * This method Show every the player's cards.
     */
    public void showCards(){
        int i=0;
        for(Animal animal:animals){
            System.out.print(i+"- ");
            System.out.println(animal);
            i++;
        }
    }

    /**
     * This method is very important.
     * Every players decides kind of attack by this method.
     *
     * @param enemy A player that is opponent of the player.
     */
    abstract public void turn(Player enemy);

    /**
     * This method checks chosen animals if has enough energy or not.
     *
     * @param chosenAnimals animals that be chosen for common attack.
     * @param sumEnergy Sun of energy that this animals like use.
     * @return If this method run return true.
     */
    public boolean commonAttackCheck(ArrayList<Animal> chosenAnimals,int sumEnergy){
        int averageEnergy=sumEnergy/chosenAnimals.size();
        boolean flag=false;
        for(int i=0 ; i<chosenAnimals.size() ; i++){
            if(chosenAnimals.get(i).checkEnergy(averageEnergy)==false){
                flag=true;
            }
        }
        if(flag==false){
            for(int i=0 ; i<chosenAnimals.size() ; i++){
                chosenAnimals.get(i).commonAttack(averageEnergy);
            }
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * This method uses for common attack.
     * @param enemy
     * @return
     */
    abstract public boolean commonAttack(Player enemy);

    /**
     * This method checks the player has at lest one animal or not
     * @return If player has animal return true.
     */
    public boolean hasAnimal(){
        if(animals.size()>0){
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * This method uses for simple attack
     * @param enemy A player that is opponent of the player.
     * @return If this method run, return true
     */
    abstract public boolean simpleAttack(Player enemy);

    /**
     * A getter for animals
     * @return animals
     */
    public ArrayList<Animal> getAnimals() {
        return animals;
    }

    /**
     * When a animal attacks, we use this method for attack to one enemy's animal.
     * @param energy energy of attack!
     * @param enemy A player that is opponent of the player.
     */
    abstract public void attackToEnemy(int energy, Player enemy);
}
