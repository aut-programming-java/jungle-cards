/**
 * This interface uses for animals that can attacking
 *
 * @author Alirea Mazochi
 * @version 1.0.0
 * @since 1397/1/11
 */

public interface Attacker {

    /**
     * This method used for attacking
     * @return amount of damage of attacking
     */
    public int attacking();

    /**
     * @return amount of damage of attacking
     */
    public int energyAttacking();


}
