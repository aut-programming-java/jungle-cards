/**
 * This class inherits of Animal
 * And implement of Killer
 */

public class Wolf extends Animal implements Killer {
    public static final int ENERGY_WOLF_KILLING=700;

    /**
     * Constructor for class Wolf
     */
    public Wolf(){
        name="Wolf";
        life=450;
        energy=700;
    }

    /**
     * This method run action 1 on a animal
     * if a animal has not enough energy for this job return 0
     * else return damage of this action
     * @return damage of this action.And if this action not available return 0.
     */
    @Override
    public int act1() {
        if(checkEnergy(ENERGY_WOLF_KILLING)){
            return killing();
        }
        return 0;
    }

    /**
     * This method run action 2 on a animal
     * if a animal has not enough energy for this job return 0
     * else return damage of this action
     * if a animal has only one action,this method return 0.
     * @return damage of this action.And if this action not available return 0.
     */
    @Override
    public int act2() {
        return 0;
    }

    /**
     * This method changes energy of the animal to first energy
     */
    @Override
    public void recoverEnergy() {
        energy=700;
    }

    /**
     * This method used for killing
     * @return amount of damage of killing
     */
    @Override
    public int killing() {
        energy-=ENERGY_WOLF_KILLING;
        return ENERGY_WOLF_KILLING;
    }

    /**
     * @return amount of damage of killing
     */
    @Override
    public int energyKilling() {
        return ENERGY_WOLF_KILLING;
    }

    /**
     * This method show details of a animal in a string
     * @return string of detail
     */
    @Override
    public String toString() {
        StringBuilder string=new StringBuilder(super.toString());
        string.append("Action 1: ");
        string.append("Killing");
        string.append("-->");
        string.append(ENERGY_WOLF_KILLING);
        string.append("\n");
        return string.toString();
    }
}
